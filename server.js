// curl -X POST -d '{"blogId": "-KBG7pqC1V62KWMBICIn"}' https://showmefish.firebaseio.com/queueBlogs/tasks.json


var Queue = require('firebase-queue'),
    Firebase = require('firebase'),
    FeedParser = require('feedparser'),
    request = require('request'),
    discover = require('rssdiscovery');

var feedparser = new FeedParser();

var ref = new Firebase('https://showmefish.firebaseio.com');

ref.authWithCustomToken('uAphOZXpum36aF0RGw5OyuRLXfNphouIXNhzT0IK', function (err, authData) {
    if (err) {
        console.log("Login failed with error: ", error);
    } else {
        console.log("Authenticated successfully with payload: ", authData);
    }
});

var queue = new Queue(ref.child("queueBlogs"), function (data, progress, resolve, reject) {
    ref.child("blog").child(data.blogId).once("value", function (blogSnapshot) {

        var blog = blogSnapshot.val();

        var req = request(blog.url);

        req.on('error', function (error) {
            console.log("Error getting", blog.url);
            reject();
        });
        req.on('response', function (res) {
            var stream = this;

            if (res.statusCode != 200) {
                reject();
            }

            stream.pipe(feedparser);
        });

        feedparser.on('error', function (error) {
            console.log("Error parsing", blog.url, error);
            reject();
        });
        feedparser.on('readable', function () {
            // This is where the action is!
            var stream = this;
            var meta = this.meta;
            var item;

            var blogMeta = {};
            if(meta.title) blogMeta.title = meta.title;
            if(meta.url) blogMeta.url = meta.url;
            if(meta.date) blogMeta.date = meta.date;
            if(meta.image) blogMeta.image = meta.image;
            if(meta.description) blogMeta.description = meta.description;
            if(meta.link) blogMeta.homepage = meta.link;

            ref.child("blog").child(data.blogId).update(blogMeta);

            ref.child("blogPost").child(data.blogId).remove(function () {
                while (item = stream.read()) {
                    if (!item.title) continue;
                    ref.child("blogPost").child(data.blogId).push({
                        "guid": item.guid,
                        "title": item.title,
                        "image": item.image,
                        "created_at": Firebase.ServerValue.TIMESTAMP,
                        "link": item.link,
                        "description": item.description,
                        "date": item.date
                    });
                }
                resolve();
            });

            console.log("Finished parsing", blog.url);
        });


    });
});

var express = require("express");
var multer = require('multer');
var path = require('path');
var crypto = require('crypto');
var fs = require('fs')
var app = express();

var imagesPath = __dirname + "/uploads/";

if (!fs.existsSync(imagesPath)){
    fs.mkdirSync(imagesPath);
}

var storage = multer.diskStorage({
    destination: imagesPath,
    filename: function (req, file, cb) {
        crypto.pseudoRandomBytes(16, function (err, raw) {
            if (err) return cb(err)

            cb(null, raw.toString('hex') + path.extname(file.originalname))
        })
    }
})
var upload = multer({storage: storage}).single('file');

// Add headers
app.use(function (req, res, next) {

    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', '*');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});

app.get('/:image', function (req, res) {
    res.sendFile(imagesPath + req.params.image);
});

app.get('/api/test', function (req, res) {
    res.end("Testing is OK");
});

app.get('/api/clear', function (req, res) {

    try {
        var files = fs.readdirSync(imagesPath);
    }
    catch (e) {
        return res.end("Error loading folder contents.");
    }    
    if (files.length > 0)
        for (var i = 0; i < files.length; i++) {
            var filePath = imagesPath + files[i];
            if (fs.statSync(filePath).isFile())
                fs.unlinkSync(filePath);
        }

    res.end("Cleared folder");
});

app.post('/api/photo', function (req, res) {
    upload(req, res, function (err) {
        if (err) {
            console.log(err);
            return res.end("Error uploading file.");
        }

        // TODO: Post processing goes here.
        res.end(JSON.stringify({status:200, data:req.file}));
    });
});

var server_port = process.env.OPENSHIFT_NODEJS_PORT || 8080
var server_ip_address = process.env.OPENSHIFT_NODEJS_IP || '127.0.0.1'

app.listen(server_port, server_ip_address, function () {
    console.log( "Listening on " + server_ip_address + ", server_port " + server_port )
});